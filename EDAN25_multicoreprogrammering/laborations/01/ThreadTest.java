public class ThreadTest  {
  public static void main(String args[]) {

    long start;
    long end;

    int size;
    int[] array;

    start = System.nanoTime();
    System.out.println("Hello!");
    end = System.nanoTime();

    size = 1000;
    array = new int[size];

    for (int i = 0; i < size; i++) {
      array[i] = i;
    }

    for (int i : array) {
      System.out.println(i);
    }


    String runtime = String.format("Elapsed time = %f s.", (end-start) / 1e9);
    System.out.println(runtime);
  }
}
